import { CATEGORY_REQUEST,CATEGORY_SUCCESS,CATEGORY_FAIL} from "../Action/ActionTypes/Types";
import Axios from "axios"


export const CategoryRequest = () => {
  return {
    type: CATEGORY_REQUEST,
  };
};
export const CategorySuccess = (data) => {
  return {
    type: CATEGORY_SUCCESS,
    payload: data,
  };
};

export const CategoryFail = (erorr) => {
  return {
    type: CATEGORY_FAIL,
    payload: erorr,
  };
};

export const featchCategorydata = () => {
  

  return (dispatch) => {
    dispatch(CategoryRequest());
   Axios.get("http://localhost:4000/categories")
      .then((response) => {
        const data = response.data;
          console.log(data);
        dispatch(CategorySuccess(data));
      })
      .catch((error) => {
        const errormsg = error.response.data;

        dispatch(CategoryFail(errormsg));
      });
  };
};
