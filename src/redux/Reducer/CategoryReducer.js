import {
  CATEGORY_REQUEST,
  CATEGORY_SUCCESS,
  CATEGORY_FAIL,

} from "../Action/ActionTypes/Types";

const InitalState = {
  loading: true,
  data: [],
  error: "",
};

const CategoryReducer = (state = InitalState, action = {}) => {
  switch (action.type) {
    case CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case CATEGORY_SUCCESS:
      return {
        loading: false,
        data: action.payload,

        error: "",
      };
    case CATEGORY_FAIL:
      return {
        loading: true,
        data: [],
        error: action.payload,
      };
 
    default:
      return state;
  }
};

export default CategoryReducer;
