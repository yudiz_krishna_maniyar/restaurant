import React from 'react';
import {Routes, Route} from 'react-router-dom'
import HomePage from '../Components/HomePage';
// import SplashScreen from '../Components/SplashScreen'

const Routeer = () => {
  return (
   <Routes>
    {/* <Route path='/' element={<SplashScreen/>}/> */}
    <Route path='/' element={<HomePage/>}/>
   </Routes>
  )
}

export default Routeer