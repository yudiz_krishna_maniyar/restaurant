import React from 'react'
import {Container} from "react-bootstrap"

const Header = () => {
  return (
    <>
      <Container>
        <div className="Kings-Arms-Cardingto">Kings Arms Cardington</div>

        <div className="-High-Street-Kem">
          134 High Street, Kempston, Bedford, Bedfordshire, MK42 7BN
        </div>
      </Container>
    </>
  );
}

export default Header