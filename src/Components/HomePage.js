import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Container, Row, Col, ThemeProvider } from "react-bootstrap";

import SplashScreen from "./SplashScreen";
import Header from "./Header";
// import Categories from './Categories';
import { featchCategorydata } from "../redux/Action/CategoryAction";

const HomePage = () => {
  const [data, setData] = useState();
  const [done, setDone] = useState(undefined);
  const dispatch = useDispatch();
    const object = {};
  const categories = useSelector((state) => state?.key?.data);
    console.log(categories);

    useEffect(() => {
    setTimeout(() => {
      dispatch(featchCategorydata(data));

      setDone(true);
    }, 2000);
  }, []);

  useEffect(() => {
    setData(categories);
  }, [categories]);


   const handleclick = ()=>{
     
       console.log(object);
     
   }

  return (
    <ThemeProvider
      breakpoints={["xxxl", "xxl", "xl", "lg", "md", "sm", "xs", "xxs"]}
      minBreakpoint="sm"
    >
      <div className="slpashscreen">
        {!done ? (
          <SplashScreen />
        ) : (
          <>
            <Header />
            {/* <Categories /> */}

            <Container onClick={() => handleclick()}>
              <Row>
                {data
                  ?.filter((i) => i.parent === null)
                  .map((filteredName) => (
                    <Col className="Rectangle">
                      <span>{filteredName.name}</span>
                    </Col>
                  ))}
              </Row>
            </Container>
          </>
        )}
        {/* {data?.forEach((i) => {
          data?.forEach((j) => {
            if (i.id === j.parent) {
              console.log(i, "===>", j);
            }
          });
        })} */}
      
    

{data?.forEach(i=>{
    return data?.forEach(j=>{
        if(i.id === j.parent){
            if(Object.keys(object).includes(JSON.stringify(i)))
            {
                object[JSON.stringify(i)].push(j)
            }
            else{
                object[JSON.stringify(i)] = [j]
            }
        }
    })
})}
    
      </div>
    </ThemeProvider>
  );
};

export default HomePage;
