import React from 'react'
import { Container, Row ,Col} from 'react-bootstrap';
import "../App.css";
import bitmap from "../Images/bitmap.png";



const SplashScreen = () => {
  return (
    <Container>
      <Row >
      <Col className='center'>
        <img className="rectangleimg" src={bitmap}></img>
        </Col>
      </Row>
    </Container>
  );
}

export default SplashScreen